# coding=utf-8

import click
import unittest

from app.main import db


@click.group()
def cli():
    pass


@cli.command()
def tests():
    """
        Runs tests for application
    """
    # unittest.main()
    pass


@cli.command()
def recreate_db():
    """
        Recreates database for application (delete/create)
    """
    db.drop_all()
    db.create_all()


if __name__ == '__main__':
    cli()

# coding=utf-8

DB_USER = 'test_user'
DB_PASSWORD = 'password1'
DB_NAME = 'intu_test'

SQLALCHEMY_DATABASE_URI_TEST = 'mysql+pymysql://{user}:{password}@{host}:{port}/{db_name}'.format(
    user=DB_USER,
    password=DB_PASSWORD,
    host='intu_db',
    port='3306',
    db_name=DB_NAME
)

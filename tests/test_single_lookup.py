# coding=utf-8

from tests import TestCase, factory


class TestSingleLookup(TestCase):
    def test_no_parameters(self):
        resp = self.client.get('/override')
        self.assertEqual(resp.status_code, 400)

    def test_non_existing_retailer(self):
        retailer = factory.retailer(self.session)
        standard_category = factory.standard_category(self.session)
        retailer_category = factory.retailer_category(
            self.session, standard_category.id, retailer.id
        )

        resp = self.client.get("/override", query_string={
            'retailer': 'ZARA',
            'category': 'Man-Clothing'
        })

        self.assertTrue(all([retailer.id, standard_category.id, retailer_category.id]))
        self.assertEqual(resp.status_code, 404)

    def test_non_existing_retailer_category(self):
        retailer = factory.retailer(self.session)
        standard_category = factory.standard_category(self.session)
        retailer_category = factory.retailer_category(self.session, standard_category.id, retailer.id)

        resp = self.client.get("/override", query_string={
            'retailer': retailer.name,
            'category': 'SomeOtherStuff'
        })

        self.assertTrue(all([retailer.id, standard_category.id, retailer_category.id]))
        self.assertEqual(resp.status_code, 404)

    def test_single_successful_lookup(self):
        standard_category = factory.standard_category(self.session)
        retailer = factory.retailer(self.session)
        retailer_category = factory.retailer_category(
            db_session=self.session,
            retailer_id=retailer.id,
            standard_category_id=standard_category.id)

        resp = self.client.get('/override', query_string={
            'retailer': retailer.name,
            'category': retailer_category.name
        })

        self.assertEqual(resp.status_code, 200)
        self.assertEqual(resp.json, {
            'retailer': retailer.name,
            'retailer_category': retailer_category.name,
            'standard_category': standard_category.name
        })

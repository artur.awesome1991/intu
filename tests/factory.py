# coding=utf-8
from copy import copy

from app import models

# TODO: Use factoryboy for this


__all__ = ('standard_category', 'retailer_category', 'retailer')


def __insert(db_session, obj):
    db_session.add(obj)
    db_session.commit()
    db_session.refresh(obj)
    # force lazy attributes retrieval
    return copy(obj)


def standard_category(db_session, name='Man', full_category_path='Man', parent_category=None):
    obj = models.StandardCategory(
        name=name,
        full_category_path=full_category_path,
        parent_category=parent_category
    )
    return __insert(db_session, obj)


def retailer_category(db_session, standard_category_id, retailer_id, name='Mans-Clothing'):
    obj = models.RetailerCategory(
        name=name,
        retailer_id=retailer_id,
        standard_category_id=standard_category_id
    )
    return __insert(db_session, obj)


def retailer(db_session, name='Amazon'):
    obj = models.Retailer(name=name)
    return __insert(db_session, obj)

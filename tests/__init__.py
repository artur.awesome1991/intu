# coding=utf-8

import unittest

from app.main import app, db
from tests import settings

app.config['SQLALCHEMY_DATABASE_URI'] = settings.SQLALCHEMY_DATABASE_URI_TEST
app.config['TESTING'] = True


class TestCase(unittest.TestCase):
    def setUp(self):
        db.create_all()
        self.session = db.session
        self.client = app.test_client()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

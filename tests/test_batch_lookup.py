# coding=utf-8
import json

from tests import TestCase, factory


class TestBatchLookup(TestCase):
    def test_successful_lookup(self):
        retailer1 = factory.retailer(self.session, name='Target')
        retailer2 = factory.retailer(self.session, name='ZARA')

        standard_category = factory.standard_category(self.session, full_category_path='Shoes')

        retailer_category1 = factory.retailer_category(self.session, standard_category.id, retailer1.id, name='_shoes')
        retailer_category2 = factory.retailer_category(self.session, standard_category.id, retailer2.id,
                                                       name='Some-Shoes')

        resp = self.client.post('/batch_override', data=json.dumps([
            {
                'retailer': retailer1.name,
                'category': retailer_category1.name
            },
            {
                'retailer': retailer2.name,
                'category': retailer_category2.name
            }
        ]), content_type='application/json')

        results = sorted(resp.json, key=lambda x: x['retailer'])

        self.assertEqual(resp.status_code, 200)
        self.assertEqual(results, [
            {
                'retailer': retailer1.name,
                'retailer_category': retailer_category1.name,
                'standard_category': standard_category.full_category_path
            },
            {
                'retailer': retailer2.name,
                'retailer_category': retailer_category2.name,
                'standard_category': standard_category.full_category_path
            }
        ])

    def test_intersecting_categories(self):
        retailer1 = factory.retailer(self.session, name='Target')
        retailer2 = factory.retailer(self.session, name='ZARA')

        standard_category = factory.standard_category(self.session)

        retailer_category1 = factory.retailer_category(self.session, standard_category.id, retailer1.id, name='Shoes')
        retailer_category2 = factory.retailer_category(self.session, standard_category.id, retailer2.id, name='Jeans')

        resp = self.client.post('/batch_override', data=json.dumps([
            {
                'retailer': retailer1.name,
                'category': retailer_category1.name
            },
            {
                'retailer': retailer1.name,
                'category': retailer_category2.name
            },
            {
                'retailer': retailer2.name,
                'category': "NonExistingOne"
            }
        ]), content_type='application/json')

        self.assertEqual(resp.status_code, 200)
        self.assertEqual(resp.json, [
            {
                'retailer': retailer1.name,
                'retailer_category': retailer_category1.name,
                'standard_category': standard_category.full_category_path
            }
        ])

    def test_empty_request(self):
        resp = self.client.post('/batch_override', data=json.dumps([]), content_type='application/json')
        self.assertEqual(resp.status_code, 400)

    def test_malformed_request(self):
        resp = self.client.post('/batch_override', data=json.dumps([
            {
                'retailer': 'Target'
            }
        ]), content_type='application/json')

        self.assertEqual(resp.status_code, 400)
create database intu_test;

create user 'test_user'@'localhost' IDENTIFIED BY 'password1';
grant all privileges ON intu_test.* TO 'test_user'@'localhost' WITH GRANT OPTION;

create user 'test_user'@'%' IDENTIFIED BY 'password1';
grant all privileges ON intu_test.* TO 'test_user'@'%' WITH GRANT OPTION;

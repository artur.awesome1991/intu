# coding=utf-8

from flask import Flask
from flask_sqlalchemy import SQLAlchemy


app = Flask(__name__)

app.config.from_object('app.settings')
db = SQLAlchemy(app)

__import__('app', fromlist=['models', 'views'])

if __name__ == "__main__":

    app.run(host='0.0.0.0', debug=True, port=80)


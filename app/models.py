# coding=utf-8

from app.main import db

from sqlalchemy import text


class Retailer(db.Model):
    __tablename__ = 'retailer'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(120), nullable=False, unique=True, index=True)
    categories = db.relationship('RetailerCategory', backref='retailer', lazy=True)


class RetailerCategory(db.Model):
    __tablename__ = 'retailer_category'
    __table_args__ = tuple(db.UniqueConstraint('name', 'retailer_id'))

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(200), nullable=False, index=True)
    retailer_id = db.Column(db.Integer, db.ForeignKey('retailer.id'), nullable=False)
    # Not sure we want to allow a non-mapped categories, so nullable is set to False
    standard_category_id = db.Column(db.Integer, db.ForeignKey('standard_category.id'), nullable=False)

    @classmethod
    def lookup_standard_category(cls, retailer_name, category_name):
        query = text("""select sc.full_category_path from retailer_category rc
            join retailer r on rc.retailer_id = r.id
            join standard_category sc on rc.standard_category_id = sc.id
            where r.name = :retailer_name and rc.name = :category_name""")
        result = db.engine.execute(query, retailer_name=retailer_name, category_name=category_name)
        row = result.fetchone()
        return row[0] if row else None

    @classmethod
    def lookup_standard_category_batch(cls, retailers, categories):
        query = text("""select r.name as retailer, rc.name as retailer_category, sc.full_category_path as standard_category 
            from retailer_category rc
            join retailer r on rc.retailer_id = r.id
            join standard_category sc on rc.standard_category_id = sc.id
            where r.name in :retailers and rc.name in :categories""")
        result = db.engine.execute(query, retailers=retailers, categories=categories)
        return list(map(lambda x: dict(x.items()), result.fetchall()))


class StandardCategory(db.Model):
    __tablename__ = 'standard_category'
    __table_args__ = tuple(db.UniqueConstraint('name', 'parent_category'))

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(200), nullable=False)
    full_category_path = db.Column(db.String(200), nullable=False, unique=True)
    parent_category = db.Column(db.Integer, nullable=True)
    retailer_categories = db.relationship('RetailerCategory', backref='standard_category', lazy=True)

# coding=utf-8

from flask import jsonify, request, abort

from app.main import app
from app.models import RetailerCategory


@app.route("/override", methods=['GET'])
def override():
    retailer = request.args.get('retailer')
    retailer_category = request.args.get('category')

    if retailer and retailer_category:
        standard_category = RetailerCategory.lookup_standard_category(
            retailer_name=retailer,
            category_name=retailer_category
        )
        if standard_category:
            return jsonify({
                'retailer': retailer,
                'retailer_category': retailer_category,
                'standard_category': standard_category
            })
        else:
            abort(404)
    else:
        abort(400)


@app.route('/batch_override', methods=['POST'])
def batch_override():
    data = request.get_json(silent=True)
    if data and _valid_batch_data(data):
        categories = RetailerCategory.lookup_standard_category_batch(
            retailers=set(item['retailer'] for item in data),
            categories=set(item['category'] for item in data)
        )
        return jsonify(_match_batch_response_to_request(request_data=data, response_data=categories))
    else:
        abort(400)


def _valid_batch_data(data):
    if not isinstance(data, list):
        return False

    for item in data:
        if 'retailer' not in item or 'category' not in item:
            return False
        if not item['retailer'] or not item['category']:
            return False
    else:
        return True


def _match_batch_response_to_request(request_data, response_data):
    out = set()

    keys_order = ('retailer', 'retailer_category', 'standard_category')

    for request_item in request_data:
        for resp_item in response_data:
            if request_item['retailer'] == resp_item['retailer'] \
                    and request_item['category'] == resp_item['retailer_category']:
                out.add((request_item['retailer'], request_item['category'], resp_item['standard_category']))

    return [dict(zip(keys_order, item)) for item in out]
